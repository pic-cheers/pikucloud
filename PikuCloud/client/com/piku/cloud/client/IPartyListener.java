package com.piku.cloud.client;

import com.piku.cloud.common.model.User;

public interface IPartyListener {

	public void onCollaboratorAdd(long partyId, User collaborator);
	
}
