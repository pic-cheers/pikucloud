package com.piku.cloud.client;

import java.io.File;
import java.util.Set;

import retrofit.Callback;

import com.piku.cloud.common.model.Credentials;
import com.piku.cloud.common.model.Party;
import com.piku.cloud.common.model.Photos;
import com.piku.cloud.common.model.User;

public interface PikuCloudClient {

	public boolean authenticate(Credentials auth);
	
	public boolean register(User user);
	
	public Party create(Party party);
	
//	public void addPartyListener(long id, IPartyListener partyListener);

	public void addCollaborator(long id, String collaboratorUsername);

	public Set<Party> loadParties(String username);

	public Party loadParty(Long partyId);
	
	public void uploadDP(File file, String username, Callback<String> callback);

	public void uploadPhoto(File file, long partyId, Callback<String> callback);
	
	public Photos fetchPhotos(long partyId);
	
}
