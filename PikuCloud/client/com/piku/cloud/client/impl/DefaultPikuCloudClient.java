package com.piku.cloud.client.impl;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import retrofit.Callback;

import com.google.gson.Gson;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.common.model.Credentials;
import com.piku.cloud.common.model.Party;
import com.piku.cloud.common.model.Photos;
import com.piku.cloud.common.model.User;

public class DefaultPikuCloudClient implements PikuCloudClient {

	private static final Map<String, String> DUMMY_JSON = new HashMap<String, String>(){
		private static final long serialVersionUID = -7259276625694538325L;
		{
			put("DUMMY", "VALUE");
		}
	};
	
	private String apiBaseUri;

	public DefaultPikuCloudClient(String apiBaseUri) {
		this.apiBaseUri = apiBaseUri;
	}

	private Client client() {
		return ClientBuilder.newClient();
	}

	private Entity<String> entity(Object obj) {
		String json = new Gson().toJson(obj);
		return Entity.entity(json, MediaType.APPLICATION_JSON);
	}
	
	private <T> T extract(String json, Class<T> clazz) {
		return new Gson().fromJson(json, clazz);
	}

	@Override
	public boolean authenticate(Credentials auth) {
		WebTarget target = client().target(apiBaseUri).path("api/users/login");
		String res = target.request().post(entity(auth), String.class);
		Boolean result = extract(res, Boolean.class);
		return result;
	}

	@Override
	public boolean register(User user) {
		WebTarget target = client().target(apiBaseUri).path("api/users/register");
		String res = target.request().post(entity(user), String.class);
		Boolean result = extract(res, Boolean.class);
		return result;
	}

	@Override
	public Party create(Party party) {
		WebTarget target = client().target(apiBaseUri).path("api/party/create");
		String res = target.request().post(entity(party), String.class);
		Long id = extract(res, Long.class);
		party.setId(id);
		return party;
	}

	/*@Override
	public void addPartyListener(long id, final IPartyListener partyListener) {
		Client client = ClientBuilder.newBuilder().build();
		WebTarget target = client.target(apiBaseUri).path("api/party/" + id);
		EventSource eventSource = new EventSource(target);
//		eventSource.open();
		EventListener listener = new EventListener() {
		    @Override public void onEvent(InboundEvent inboundEvent) {
		    	switch (inboundEvent.getName()) {
		    		case "party/notifyCollaboratorAddition":
		    			String json = inboundEvent.readData(String.class);
		    			User collaborator = extract(json, User.class);
	    				String id = inboundEvent.getId();
	    				Long partyId = Long.parseLong(id);
		    			partyListener.onCollaboratorAdd(partyId, collaborator);
		    			break;
		    		
		    		default:
		    	}
		    }
		};
		eventSource.register(listener); 
	}*/

	@Override
	public void addCollaborator(long id, String collaboratorUsername) {
		WebTarget target = client().target(apiBaseUri)
			.path("api/party/" + id + "/addcollaborator/" + collaboratorUsername);
		target.request().post(entity(DUMMY_JSON), String.class);
	}
	
	public static void main(String[] args) {
//		testUserAPI();
		testPartyAPI();
	}

	private static void testPartyAPI() {
		PikuCloudClient pcc = new DefaultPikuCloudClient("http://localhost:8080/PikuCloud");
		Party party = new Party();
		party.setName("Venil's Party");
		party.setOwnerUsername("venil");
		party.setCreatedAt(new Date());
		party.setCollaborators(Arrays.asList(new String[] { "venil" }));
		Party create = pcc.create(party);
		System.out.println(new Gson().toJson(create));
		/*pcc.addPartyListener(1, new IPartyListener() {
			@Override public void onCollaboratorAdd(long partyId, User collaborator) {
				System.out.println("User " + new Gson().toJson(collaborator)
						+ " added to party " + partyId + ".");
			}
		});*/
		pcc.addCollaborator(1, "donna");
	}

	@SuppressWarnings("unused")
	private static void testUserAPI() {
		PikuCloudClient pcc = new DefaultPikuCloudClient("http://localhost:8080/PikuCloud");
		User user = new User();
		user.setUsername("asd");
		user.setName("asd asd");
		user.setPasswordHash("asd");
		boolean success = pcc.register(user);
		Credentials auth = new Credentials();
		auth.setUsername("asd");
		auth.setPasswordHash("asd");
		boolean loggedIn = pcc.authenticate(auth);
		System.out.println("registered " + success + " loggedIn: " + loggedIn);
	}

	@Override
	public Set<Party> loadParties(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Party loadParty(Long partyId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void uploadDP(File file, String desc, Callback<String> callback) {
		// TODO Auto-generated method stub
	}

	@Override
	public void uploadPhoto(File file, long partyId, Callback<String> callback) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Photos fetchPhotos(long partyId) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
