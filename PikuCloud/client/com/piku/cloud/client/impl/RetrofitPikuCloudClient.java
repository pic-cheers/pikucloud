package com.piku.cloud.client.impl;

import java.io.File;
import java.util.Set;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.mime.TypedFile;

import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.common.model.Credentials;
import com.piku.cloud.common.model.Party;
import com.piku.cloud.common.model.Photos;
import com.piku.cloud.common.model.User;

public class RetrofitPikuCloudClient implements PikuCloudClient {

	private String apiBaseUri;

	public RetrofitPikuCloudClient(String apiBaseUri) {
		this.apiBaseUri = apiBaseUri;
	}

	private RestAdapter newRestAdapter() {
		RestAdapter restAdapter = new RestAdapter.Builder()
		  						  .setEndpoint(apiBaseUri)
		  						  .build();
		return restAdapter;
	}

	@Override
	public boolean authenticate(Credentials auth) {
		PikuService pcc = newRestAdapter().create(PikuService.class);
		boolean authenticated = pcc.authenticate(auth);
		return authenticated;
	}

	@Override
	public boolean register(User user) {
		PikuService pcc = newRestAdapter().create(PikuService.class);
		boolean success = pcc.register(user);
		return success;
	}

	@Override
	public Party create(Party party) {
		PikuService pcc = newRestAdapter().create(PikuService.class);
		long id = pcc.create(party);
		party.setId(id);
		return party;
	}

	/*@Override
	public void addPartyListener(long id, final IPartyListener partyListener) {
		Client client = ClientBuilder.newBuilder().build();
		WebTarget target = client.target(apiBaseUri).path("api/party/" + id);
		EventSource eventSource = new EventSource(target);
//		eventSource.open();
		EventListener listener = new EventListener() {
		    @Override public void onEvent(InboundEvent inboundEvent) {
		    	switch (inboundEvent.getName()) {
		    		case "party/notifyCollaboratorAddition":
		    			String json = inboundEvent.readData(String.class);
		    			User collaborator = extract(json, User.class);
	    				String id = inboundEvent.getId();
	    				Long partyId = Long.parseLong(id);
		    			partyListener.onCollaboratorAdd(partyId, collaborator);
		    			break;
		    		
		    		default:
		    	}
		    }
		};
		eventSource.register(listener); 
	}*/

	@Override
	public void addCollaborator(long id, String collaboratorUsername) {
		PikuService pcc = newRestAdapter().create(PikuService.class);
		pcc.addCollaborator(id, collaboratorUsername);
	}

	@Override
	public Set<Party> loadParties(String username) {
		PikuService pcc = newRestAdapter().create(PikuService.class);
		Set<Party> parties = pcc.loadParties(username);
		return parties;
	}
	
	public static void main(String[] args) {
		testUserAPI();
//		testPartyAPI();
	}

	/*private static void testPartyAPI() {
		PikuCloudClient pcc = new RetrofitPikuCloudClient("http://localhost:8080/PikuCloud");
		Party party = new Party();
		party.setName("Venil's Party");
		party.setOwnerUsername("venil");
		party.setCreatedAt(new Date());
		party.setCollaborators(Arrays.asList(new String[] { "venil" }));
		Party create = pcc.create(party);
		System.out.println(new Gson().toJson(create));
		pcc.addPartyListener(1, new IPartyListener() {
			@Override public void onCollaboratorAdd(long partyId, User collaborator) {
				System.out.println("User " + new Gson().toJson(collaborator)
						+ " added to party " + partyId + ".");
			}
		});
		pcc.addCollaborator(1, "donna");
	}*/

	private static void testUserAPI() {
		PikuCloudClient pcc = new RetrofitPikuCloudClient("http://localhost:8080/PikuCloud");
		User user = new User();
		user.setUsername("asd");
		user.setName("asd asd");
		user.setPasswordHash("asd");
		boolean success = pcc.register(user);
		Credentials auth = new Credentials();
		auth.setUsername("asd");
		auth.setPasswordHash("asd");
		boolean loggedIn = pcc.authenticate(auth);
		System.out.println("registered " + success + " loggedIn: " + loggedIn);
	}

	@Override
	public Party loadParty(Long partyId) {
		PikuService pcc = newRestAdapter().create(PikuService.class);
		return pcc.loadParty(partyId);
	}

	@Override
	public void uploadDP(File file, String username, Callback<String> callback) {
		String BASE_URL = apiBaseUri + "/api/users/profile";
		FileUpload service = ServiceGenerator.createService(FileUpload.class, BASE_URL);
		TypedFile typedFile = new TypedFile("multipart/form-data", file);
		service.upload(typedFile, username, callback);
	}

	@Override
	public void uploadPhoto(File file, long partyId, Callback<String> callback) {
		String BASE_URL = apiBaseUri + "/api/photos";
		FileUpload service = ServiceGenerator.createService(FileUpload.class, BASE_URL);
		TypedFile typedFile = new TypedFile("multipart/form-data", file);
		service.upload(typedFile, String.valueOf(partyId), callback);
	}

	@Override
	public Photos fetchPhotos(long partyId) {
		PikuService pcc = newRestAdapter().create(PikuService.class);
		return pcc.fetchPhotos(partyId);
	}
	
}
