package com.piku.cloud.client.impl;

import java.util.Set;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.piku.cloud.common.model.Credentials;
import com.piku.cloud.common.model.Party;
import com.piku.cloud.common.model.Photos;
import com.piku.cloud.common.model.User;

public interface PikuService {

	@POST("/api/users/login")
	public boolean authenticate(@Body Credentials auth);
	
	@POST("/api/users/register")
	public boolean register(@Body User user);
	
	@POST("/api/party/create")
	public long create(@Body Party party);
	
	@GET("/api/party/{partyId}/addcollaborator/{collaboratorUsername}")
	public String addCollaborator(@Path("partyId") long partyId, @Path("collaboratorUsername") String collaboratorUsername);
	
	@GET("/api/users/{username}/parties")
	public Set<Party> loadParties(@Path("username") String username);

	@POST("/api/party/load")
	public Party loadParty(@Body Long pId);

	@GET("/api/photos/{partyId}")
	public Photos fetchPhotos(@Path("partyId") long partyId);

}
