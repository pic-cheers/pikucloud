package com.piku.cloud.client.impl;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

import com.squareup.okhttp.OkHttpClient;

public class ServiceGenerator {

	// No need to instantiate this class.
	private ServiceGenerator() {
	}

	public static <S> S createService(Class<S> serviceClass, String baseUrl) {
		RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(
				baseUrl).setClient(new OkClient(new OkHttpClient()));
		RestAdapter adapter = builder.build();
		return adapter.create(serviceClass);
	}

}