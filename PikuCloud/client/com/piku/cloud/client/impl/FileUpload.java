package com.piku.cloud.client.impl;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

public interface FileUpload {

	@Multipart
    @POST("/upload")
    public void upload(@Part("file") TypedFile file, @Part("description") String description, Callback<String> cb);
	
}
