package com.piku.cloud.inf;

public interface IConverter<A, B> {

	public A convertTo(B obj);
	
	public B convertFrom(A obj);
	
}
