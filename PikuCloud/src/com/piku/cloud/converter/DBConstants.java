package com.piku.cloud.converter;

public class DBConstants {

	public static final String USERNAME = "username";
	public static final String NAME = "name";
	public static final String PASSWORD = "password";
	public static final String ID = "id";
	public static final String OWNER_USERNAME = "ownerUsername";
	public static final String CREATED_AT = "createdAt";
	public static final String COLLABORATORS = "collaborators";
	public static final String FILE_PATH = "filePath";
	public static final String PARTY_ID = "partyId";
	public static final String PHOTO_URLS = "photoUrls";
	
}
