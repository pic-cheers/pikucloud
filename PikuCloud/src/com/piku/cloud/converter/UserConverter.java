package com.piku.cloud.converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.piku.cloud.common.model.User;
import com.piku.cloud.inf.IConverter;

public class UserConverter implements IConverter<DBObject, User> {

	@Override
	public DBObject convertTo(User obj) {
		DBObject dbo = new BasicDBObject();
		dbo.put(DBConstants.USERNAME, obj.getUsername());
		dbo.put(DBConstants.NAME, obj.getName());
		dbo.put(DBConstants.PASSWORD, obj.getPasswordHash());
		dbo.put(DBConstants.FILE_PATH, obj.getFilePath());
		return dbo;
	}

	@Override
	public User convertFrom(DBObject obj) {
		User user = new User();
		user.setUsername((String) obj.get(DBConstants.USERNAME));
		user.setName((String) obj.get(DBConstants.NAME));
		user.setPasswordHash((String) obj.get(DBConstants.PASSWORD));
		user.setFilePath((String) obj.get(DBConstants.FILE_PATH));
		return user;
	}

}
