package com.piku.cloud.converter;

import java.util.Date;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.piku.cloud.common.model.Party;
import com.piku.cloud.inf.IConverter;

public class PartyConverter implements IConverter<DBObject, Party> {

	@Override
	public DBObject convertTo(Party obj) {
		DBObject dbo = new BasicDBObject();
		dbo.put(DBConstants.ID, obj.getId());
		dbo.put(DBConstants.NAME, obj.getName());
		dbo.put(DBConstants.OWNER_USERNAME, obj.getOwnerUsername());
		dbo.put(DBConstants.CREATED_AT, obj.getCreatedAt());
		dbo.put(DBConstants.COLLABORATORS, obj.getCollaborators());
		return dbo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Party convertFrom(DBObject obj) {
		Party party = new Party();
		party.setId((long) obj.get(DBConstants.ID));
		party.setName((String) obj.get(DBConstants.NAME));
		party.setOwnerUsername((String) obj.get(DBConstants.OWNER_USERNAME));
		party.setCreatedAt((Date) obj.get(DBConstants.CREATED_AT));
		party.setCollaborators((List<String>) obj.get(DBConstants.COLLABORATORS));
		return party;
	}

}
