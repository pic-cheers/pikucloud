package com.piku.cloud.converter;

import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.piku.cloud.common.model.Photos;
import com.piku.cloud.inf.IConverter;

public class PhotosConverter implements IConverter<DBObject, Photos> {

	@Override
	public DBObject convertTo(Photos obj) {
		DBObject dbo = new BasicDBObject();
		dbo.put(DBConstants.PARTY_ID, obj.getPartyId());
		dbo.put(DBConstants.PHOTO_URLS, obj.getPhotoUrls());
		return dbo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Photos convertFrom(DBObject obj) {
		Photos photos = new Photos();
		photos.setPartyId((long) obj.get(DBConstants.PARTY_ID));
		photos.setPhotoUrls((List<String>) obj.get(DBConstants.PHOTO_URLS));
		return photos;
	}

}
