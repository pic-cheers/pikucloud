package com.piku.cloud.business.inf;

import com.piku.cloud.business.exception.DuplicateUserException;
import com.piku.cloud.common.model.Credentials;
import com.piku.cloud.common.model.User;

public interface IUserManager {

	/**
	 * Returns <code>true</code> if authentication is successful.
	 */
	public boolean authenticate(Credentials credentials);

	/**
	 * @throws DuplicateUserException
	 */
	public void register(User user) throws DuplicateUserException;

	public void updateDPPath(String username, String filePath);
	
}
