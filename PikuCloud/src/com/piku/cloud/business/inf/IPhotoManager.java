package com.piku.cloud.business.inf;

import com.piku.cloud.common.model.Photos;

public interface IPhotoManager {

	public void addPhoto(long parseLong, String photoUrl);

	public Photos fetchPhotos(long partyId);

}
