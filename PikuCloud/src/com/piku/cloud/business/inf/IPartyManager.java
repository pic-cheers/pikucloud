package com.piku.cloud.business.inf;

import java.util.Set;

import org.glassfish.jersey.media.sse.EventOutput;

import com.piku.cloud.common.model.Party;

public interface IPartyManager {

	/**
	 * Returns id of the created party.
	 */
	public long create(Party party);

	/**
	 * Adds the given collaborator to the given party.
	 */
	public void addCollaborator(long partyId, String collaboratorUsername);
	
	/**
	 * Listens to the given partyId via SSE.
	 */
	public void listenToParty(long partyId, EventOutput eventOutput);

	/**
	 * Fetches all parties attended by user.
	 */
	public Set<Party> loadParties(String username);

	/**
	 * Loads a particular party by id.
	 */
	public Party loadParty(long partyId);

}
