package com.piku.cloud.business.inf;

import com.piku.cloud.business.domain.PartyMember;
import com.piku.cloud.common.model.User;

public interface IPartyEventBroadcaster {

	public void onListenerAdd(long partyId, PartyMember partyMember);
	
	public void onCollboratorAdd(long partyId, User collaborator);
	
}
