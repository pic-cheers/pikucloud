package com.piku.cloud.business.domain;

import java.io.IOException;

import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;

import com.google.gson.Gson;
import com.piku.cloud.common.model.User;

public class PartyMember {

	private long partyId;
	private EventOutput eventOutput;

	public PartyMember(long partyId, EventOutput eventOutput) {
		this.partyId = partyId;
		this.eventOutput = eventOutput;
	}
	
	public void notifyCollaboratorAddition(User collaborator) throws IOException {
		final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
		eventBuilder.name("party/notifyCollaboratorAddition");
		eventBuilder.id(String.valueOf(partyId));
		eventBuilder.data(String.class, new Gson().toJson(collaborator));
		final OutboundEvent event = eventBuilder.build();
		eventOutput.write(event);
	}

}
