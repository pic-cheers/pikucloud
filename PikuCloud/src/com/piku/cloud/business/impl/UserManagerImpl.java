package com.piku.cloud.business.impl;

import com.piku.cloud.business.exception.DuplicateUserException;
import com.piku.cloud.business.inf.IUserManager;
import com.piku.cloud.common.model.Credentials;
import com.piku.cloud.common.model.User;
import com.piku.cloud.dao.IUserDAO;

public class UserManagerImpl implements IUserManager {

	private IUserDAO userDAO;

	public UserManagerImpl(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public boolean authenticate(Credentials credentials) {
		User user = userDAO.findOne(credentials.getUsername(), credentials.getPasswordHash());
		return user != null;
	}

	@Override
	public void register(User user) throws DuplicateUserException {
		if (userDAO.exists(user)) {
			throw new DuplicateUserException();
		}
		userDAO.insert(user);
	}

	@Override
	public void updateDPPath(String username, String filePath) {
		userDAO.updateDPPath(username, filePath);
	}

}
