package com.piku.cloud.business.impl;

import java.util.Set;

import org.glassfish.jersey.media.sse.EventOutput;

import com.piku.cloud.business.domain.PartyMember;
import com.piku.cloud.business.inf.IPartyEventBroadcaster;
import com.piku.cloud.business.inf.IPartyManager;
import com.piku.cloud.common.model.Party;
import com.piku.cloud.common.model.User;
import com.piku.cloud.dao.IPartyDAO;
import com.piku.cloud.dao.IUserDAO;

public class PartyManagerImpl implements IPartyManager {

	private IPartyDAO partyDAO;
	private IUserDAO userDAO;
	private IPartyEventBroadcaster eventBroadcaster;

	public PartyManagerImpl(IPartyDAO partyDAO, IUserDAO userDAO) {
		this.partyDAO = partyDAO;
		this.userDAO = userDAO;
		this.eventBroadcaster = new PartyEventBroadcasterImpl();
	}

	@Override
	public long create(Party party) {
		return partyDAO.create(party);
	}

	@Override
	public void listenToParty(long partyId, EventOutput eventOutput) {
		eventBroadcaster.onListenerAdd(partyId, new PartyMember(partyId, eventOutput));
	}

	@Override
	public void addCollaborator(long partyId, String collaboratorUsername) {
		partyDAO.addCollaborator(partyId, collaboratorUsername);
		User user = userDAO.findOne(collaboratorUsername);
		eventBroadcaster.onCollboratorAdd(partyId, user);
	}

	@Override
	public Set<Party> loadParties(String username) {
		Set<Party> parties = partyDAO.loadParties(username);
		return parties;
	}

	@Override
	public Party loadParty(long partyId) {
		return partyDAO.loadParty(partyId);
	}
	
}
