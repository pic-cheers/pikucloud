package com.piku.cloud.business.impl;

import com.piku.cloud.business.inf.IPhotoManager;
import com.piku.cloud.common.model.Photos;
import com.piku.cloud.dao.IPhotosDAO;

public class PhotosManagerImpl implements IPhotoManager {

	private IPhotosDAO photosDAO;

	public PhotosManagerImpl(IPhotosDAO photosDAO) {
		this.photosDAO = photosDAO;
	}
	
	@Override
	public void addPhoto(long partyId, String photoUrl) {
		photosDAO.add(partyId, photoUrl);
	}
	
	@Override
	public Photos fetchPhotos(long partyId) {
		return photosDAO.fetchPhotos(partyId);
	}

}
