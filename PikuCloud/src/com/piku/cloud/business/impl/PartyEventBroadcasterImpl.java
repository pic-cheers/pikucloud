package com.piku.cloud.business.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.piku.cloud.business.domain.PartyMember;
import com.piku.cloud.business.inf.IPartyEventBroadcaster;
import com.piku.cloud.common.model.User;

public class PartyEventBroadcasterImpl implements IPartyEventBroadcaster {

	private Map<Long, List<PartyMember>> partyMembers = new HashMap<Long, List<PartyMember>>();

	@Override
	public void onListenerAdd(long partyId, PartyMember partyMember) {
		List<PartyMember> members = partyMembers.get(partyId);
		if (members == null) {
			members = new ArrayList<PartyMember>();
			partyMembers.put(partyId, members);
		}
		members.add(partyMember);
	}
	
	@Override
	public void onCollboratorAdd(long partyId, User collaborator) {
		List<PartyMember> members = partyMembers.get(partyId);
		if (members != null) {
			Iterator<PartyMember> it = members.iterator();
			while (it.hasNext()) {
				PartyMember member = it.next();
				try {
					member.notifyCollaboratorAddition(collaborator);
				}
				catch (IOException e) {
					System.out.println("Removed party member from party " + partyId);
					e.printStackTrace();
					it.remove();
				}
			}
		}
	}

}
