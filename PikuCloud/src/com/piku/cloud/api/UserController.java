package com.piku.cloud.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.Gson;
import com.piku.cloud.boot.Services;
import com.piku.cloud.business.exception.DuplicateUserException;
import com.piku.cloud.business.inf.IPartyManager;
import com.piku.cloud.business.inf.IUserManager;
import com.piku.cloud.common.model.Credentials;
import com.piku.cloud.common.model.Party;
import com.piku.cloud.common.model.User;

@Path("/users")
public class UserController {

	private IUserManager userManager = Services.get(Services.USER_MANAGER);
	private IPartyManager partyManager = Services.get(Services.PARTY_MANAGER);

	private <T> T extract(String json, Class<T> clazz) {
		return new Gson().fromJson(json, clazz);
	}

	private String json(Object obj) {
		return new Gson().toJson(obj);
	}

	@Path("/login")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(String json) {
		Credentials credentials = extract(json, Credentials.class);
		boolean authenticate = userManager.authenticate(credentials);
		return Response.status(200).entity(json(authenticate)).build();
	}

	@Path("/register")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response register(String json) {
		User user = extract(json, User.class);
		boolean success = true;
		try {
			userManager.register(user);
		} catch (DuplicateUserException e) {
			success = false;
			e.printStackTrace();
		}
		return Response.status(200).entity(json(success)).build();
	}

	@Path("/{username}/parties")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadParties(@PathParam("username") String username) {
		Set<Party> parties = partyManager.loadParties(username);
		return Response.status(200).entity(json(parties)).build();
	}

	@Path("/profile/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public Response uploadDP(
			@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
			@FormDataParam("description") String username) {
		URL r = this.getClass().getResource("/");
		String clsDir = null;
		try {
			clsDir = URLDecoder.decode(r.getFile(), "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String filename = contentDispositionHeader.getFileName();
		int lastIndexOfDot = filename.lastIndexOf('.');
		String ext = filename.substring(lastIndexOfDot);
		String filePath = clsDir + "attachments" + File.separator;
		filePath = filePath.replaceAll(File.separator + "WEB-INF" + File.separator + "classes", "");
		String fileNameExt = "profile-" + username + ext;
		saveFile(fileInputStream, filePath, fileNameExt);
		String output = "File saved to server location: " + filePath;
		userManager.updateDPPath(username, "attachments" + File.separator + fileNameExt);
		return Response.status(200).entity(output).build();
	}
	
	private void saveFile(InputStream uploadedInputStream, String filePath, String fileName) {
		try {
			File file = new File(filePath);
			file.mkdirs();
			file = new File(filePath + fileName);
			file.setLastModified(System.currentTimeMillis());
			OutputStream outpuStream = new FileOutputStream(file);
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
