package com.piku.cloud.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.piku.cloud.boot.Services;
import com.piku.cloud.business.inf.IPartyManager;
import com.piku.cloud.common.model.Party;

@Path("/party")
public class PartyController {
	
	private IPartyManager partyManager = Services.get(Services.PARTY_MANAGER);

	private <T> T extract(String json, Class<T> clazz) {
		return new Gson().fromJson(json, clazz);
	}

	private String json(Object obj) {
		return new Gson().toJson(obj);
	}
	

	@Path("/load")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadParty(String partyIdStr) {
		long partyId = Long.parseLong(partyIdStr);
		Party party = partyManager.loadParty(partyId);
		return Response.status(200).entity(json(party)).build();
	}
	
	@Path("/create")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String json) {
		Party party = extract(json, Party.class);
		long id = partyManager.create(party);
		return Response.status(200).entity(json(id)).build();
	}
	
	@Path("{partyId}/addcollaborator/{collaboratorUsername}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCollaborator(@PathParam("partyId") long partyId,
			@PathParam("collaboratorUsername") String collaboratorUsername) {
		partyManager.addCollaborator(partyId, collaboratorUsername);
		return Response.status(200).build();
	}

	/*@Path("{partyId}")
	@GET
	@Produces(SseFeature.SERVER_SENT_EVENTS)
	public EventOutput listenToParty(@PathParam("partyId") long partyId) {
		final EventOutput eventOutput = new EventOutput();
		partyManager.listenToParty(partyId, eventOutput);
		return eventOutput;
	}*/
	
}
