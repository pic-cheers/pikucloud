package com.piku.cloud.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.Gson;
import com.piku.cloud.boot.Services;
import com.piku.cloud.business.inf.IPhotoManager;
import com.piku.cloud.common.model.Photos;

@Path("/photos")
public class PhotoController {

	private IPhotoManager photoManager = Services.get(Services.PHOTOS_MANAGER);
	
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public Response uploadDP(
			@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
			@FormDataParam("description") String partyId) {
		System.out.println("Photo uploaded " + partyId);
		URL r = this.getClass().getResource("/");
		String clsDir = null;
		try {
			clsDir = URLDecoder.decode(r.getFile(), "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String filename = contentDispositionHeader.getFileName();
		int lastIndexOfDot = filename.lastIndexOf('.');
		String ext = filename.substring(lastIndexOfDot);
		String filePath = clsDir + "attachments" + File.separator + partyId + File.separator;
		filePath = filePath.replaceAll(File.separator + "WEB-INF" + File.separator + "classes", "");
		String fileNameExt = "photo-" + System.currentTimeMillis() + ext;
		saveFile(fileInputStream, filePath, fileNameExt);
		String output = "File saved to server location: " + filePath;
		photoManager.addPhoto(Long.parseLong(partyId), "attachments"
				+ File.separator + partyId + File.separator + fileNameExt);
		return Response.status(200).entity(output).build();
	}
	
	private void saveFile(InputStream uploadedInputStream, String filePath, String fileName) {
		try {
			File file = new File(filePath);
			file.mkdirs();
			file = new File(filePath + fileName);
			file.setLastModified(System.currentTimeMillis());
			OutputStream outpuStream = new FileOutputStream(file);
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String json(Object obj) {
		return new Gson().toJson(obj);
	}

	@Path("{partyId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCollaborator(@PathParam("partyId") long partyId) {
		System.out.println("Photo fetched " + partyId);
		Photos photos = photoManager.fetchPhotos(partyId);
		return Response.status(200).entity(json(photos)).build();
	}

}
