package com.piku.cloud.boot;

public class AppConfig {

	public String dbUrl;
	public String dbName;
	public String usersCollection;
	public String partiesCollection;
	public String photosCollection;

}
