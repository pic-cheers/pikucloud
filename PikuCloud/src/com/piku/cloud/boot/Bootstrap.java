package com.piku.cloud.boot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.glassfish.jersey.server.ResourceConfig;
import org.yaml.snakeyaml.Yaml;

public class Bootstrap extends ResourceConfig {

	public static final String PIKU_CLOUD_CONF = System.getenv("EXPLIC8_HOME");
	
	public Bootstrap() throws FileNotFoundException {
		AppConfig appConfig = new Yaml().loadAs(new FileInputStream(
				PIKU_CLOUD_CONF + File.separator + "appConfig.yml"), AppConfig.class);
		Services.init(appConfig);
	}
	
}
