package com.piku.cloud.boot;

import java.util.HashMap;
import java.util.Map;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.piku.cloud.business.impl.PartyManagerImpl;
import com.piku.cloud.business.impl.PhotosManagerImpl;
import com.piku.cloud.business.impl.UserManagerImpl;
import com.piku.cloud.business.inf.IPartyManager;
import com.piku.cloud.business.inf.IPhotoManager;
import com.piku.cloud.business.inf.IUserManager;
import com.piku.cloud.dao.IPartyDAO;
import com.piku.cloud.dao.IPhotosDAO;
import com.piku.cloud.dao.IUserDAO;
import com.piku.cloud.dao.impl.PartyDAOImpl;
import com.piku.cloud.dao.impl.PhotosDAOImpl;
import com.piku.cloud.dao.impl.UserDAOImpl;

public class Services {

	public static final String USER_MANAGER = "USER_MANAGER";
	public static final String PARTY_MANAGER = "PARTY_MANAGER";
	public static final String PHOTOS_MANAGER = "PHOTO_MANAGER";
	
	private static Map<String, Object> services = new HashMap<String, Object>();
	
	public static AppConfig appConfig;
	
	private static DBCollection usersColl;
	private static IUserDAO userDAO;
	private static DBCollection partiesColl;
	private static IPartyDAO partyDAO;
	private static DBCollection photosColl;
	private static IPhotosDAO photosDAO;

	public static void init(AppConfig appConfig) {
		Services.appConfig = appConfig;
		initMongo();
		initDAOs();
		initServices();
	}

	@SuppressWarnings({"resource", "deprecation" })
	private static void initMongo() {
		MongoClient mc = new MongoClient(appConfig.dbUrl);
		DB db = mc.getDB(appConfig.dbName);
		usersColl = db.getCollection(appConfig.usersCollection);
		partiesColl = db.getCollection(appConfig.partiesCollection);
		photosColl = db.getCollection(appConfig.photosCollection);
	}

	private static void initDAOs() {
		userDAO = new UserDAOImpl(usersColl);
		partyDAO = new PartyDAOImpl(partiesColl);
		photosDAO = new PhotosDAOImpl(photosColl);
	}

	private static void initServices() {
		IUserManager userManager = new UserManagerImpl(userDAO);
		services.put(USER_MANAGER, userManager);
		IPartyManager partyManager = new PartyManagerImpl(partyDAO, userDAO);
		services.put(PARTY_MANAGER, partyManager);
		IPhotoManager photosManager = new PhotosManagerImpl(photosDAO);
		services.put(PHOTOS_MANAGER, photosManager);
	}
	
	@SuppressWarnings("unchecked")
	public static <S> S get(String name) {
		return (S) services.get(name);
	}

}
