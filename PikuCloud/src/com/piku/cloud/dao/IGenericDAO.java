package com.piku.cloud.dao;

import java.util.Set;

import com.mongodb.DBObject;

public interface IGenericDAO<T> {

	public T findOne(DBObject query);
	
	public void insert(T object);

	public long count();

	public void findAndModify(DBObject query, T object);

	public Set<T> findAll(DBObject query, DBObject sort);

}
