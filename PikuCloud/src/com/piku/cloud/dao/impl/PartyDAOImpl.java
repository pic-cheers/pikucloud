package com.piku.cloud.dao.impl;

import java.util.Set;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.piku.cloud.common.model.Party;
import com.piku.cloud.converter.DBConstants;
import com.piku.cloud.converter.PartyConverter;
import com.piku.cloud.dao.GenericDAOImpl;
import com.piku.cloud.dao.IGenericDAO;
import com.piku.cloud.dao.IPartyDAO;

public class PartyDAOImpl implements IPartyDAO {

	private IGenericDAO<Party> dao;
	
	public PartyDAOImpl(DBCollection collection) {
		this.dao = new GenericDAOImpl<Party>(collection, new PartyConverter());
	}

	@Override
	public long create(Party party) {
		long id = dao.count() + 1;
		party.setId(id);
		dao.insert(party);
		return id;
	}

	@Override
	public void addCollaborator(long partyId, String collaboratorUsername) {
		DBObject query = new BasicDBObject();
		query.put(DBConstants.ID, partyId);
		Party party = dao.findOne(query);
		if (!party.getCollaborators().contains(collaboratorUsername)) {
			party.getCollaborators().add(collaboratorUsername);
		}
		dao.findAndModify(query, party);
	}

	@Override
	public Set<Party> loadParties(String collaboratorUsername) {
		DBObject query = new BasicDBObject();
		query.put(DBConstants.COLLABORATORS, collaboratorUsername);
		DBObject sort = new BasicDBObject();
		sort.put(DBConstants.CREATED_AT, -1);
		Set<Party> parties = dao.findAll(query, sort);
		return parties;
	}

	@Override
	public Party loadParty(long partyId) {
		DBObject query = new BasicDBObject();
		query.put(DBConstants.ID, partyId);
		Party party = dao.findOne(query);
		return party;
	}

}
