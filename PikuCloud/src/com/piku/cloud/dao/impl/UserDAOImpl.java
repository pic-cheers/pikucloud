package com.piku.cloud.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.piku.cloud.common.model.User;
import com.piku.cloud.converter.DBConstants;
import com.piku.cloud.converter.UserConverter;
import com.piku.cloud.dao.GenericDAOImpl;
import com.piku.cloud.dao.IGenericDAO;
import com.piku.cloud.dao.IUserDAO;

public class UserDAOImpl implements IUserDAO {

	private IGenericDAO<User> dao;
	
	public UserDAOImpl(DBCollection collection) {
		this.dao = new GenericDAOImpl<User>(collection, new UserConverter());
	}
	
	@Override
	public User findOne(String username, String passwordHash) {
		DBObject query = new BasicDBObject();
		query.put(DBConstants.USERNAME, username);
		query.put(DBConstants.PASSWORD, passwordHash);
		User user = dao.findOne(query);
		return user;
	}

	@Override
	public void insert(User user) {
		dao.insert(user);
	}

	@Override
	public boolean exists(User user) {
		User dbUser = findOne(user.getUsername());
		return dbUser != null;
	}

	@Override
	public User findOne(String username) {
		DBObject query = new BasicDBObject();
		query.put(DBConstants.USERNAME, username);
		User dbUser = dao.findOne(query);
		return dbUser;
	}

	@Override
	public void updateDPPath(String username, String filePath) {
		User user = findOne(username);
		user.setFilePath(filePath);
		DBObject query = new BasicDBObject();
		query.put(DBConstants.USERNAME, username);
		dao.findAndModify(query, user);
	}

}
