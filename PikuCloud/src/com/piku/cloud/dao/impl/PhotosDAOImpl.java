package com.piku.cloud.dao.impl;

import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.piku.cloud.common.model.Photos;
import com.piku.cloud.converter.DBConstants;
import com.piku.cloud.converter.PhotosConverter;
import com.piku.cloud.dao.GenericDAOImpl;
import com.piku.cloud.dao.IGenericDAO;
import com.piku.cloud.dao.IPhotosDAO;

public class PhotosDAOImpl implements IPhotosDAO {

	private IGenericDAO<Photos> dao;
	
	public PhotosDAOImpl(DBCollection collection) {
		this.dao = new GenericDAOImpl<Photos>(collection, new PhotosConverter());
	}

	@Override
	public void add(long partyId, String photoUrl) {
		DBObject query = new BasicDBObject();
		query.put(DBConstants.PARTY_ID, partyId);
		Photos photos = dao.findOne(query);
		if (photos == null) {
			photos = new Photos();
			photos.setPartyId(partyId);
			photos.setPhotoUrls(new ArrayList<String>());
			photos.getPhotoUrls().add(photoUrl);
			dao.insert(photos);
		}
		else {
			photos.getPhotoUrls().add(photoUrl);
			dao.findAndModify(query, photos);
		}
	}

	@Override
	public Photos fetchPhotos(long partyId) {
		DBObject query = new BasicDBObject();
		query.put(DBConstants.PARTY_ID, partyId);
		Photos photos = dao.findOne(query);
		if (photos == null) {
			photos = new Photos();
			photos.setPartyId(partyId);
			photos.setPhotoUrls(new ArrayList<String>());
		}
		return photos;
	}

}
