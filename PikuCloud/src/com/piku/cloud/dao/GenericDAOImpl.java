package com.piku.cloud.dao;

import java.util.LinkedHashSet;
import java.util.Set;

import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.piku.cloud.inf.IConverter;

public class GenericDAOImpl<T> implements IGenericDAO<T> {

	private DBCollection collection;
	private IConverter<DBObject, T> converter;

	public GenericDAOImpl(DBCollection collection,
			IConverter<DBObject, T> converter) {
		this.collection = collection;
		this.converter = converter;
	}

	@Override
	public T findOne(DBObject query) {
		DBObject object = collection.findOne(query);
		T t = null;
		if (object != null) {
			t = converter.convertFrom(object);
		}
		return t;
	}

	@Override
	public void insert(T object) {
		DBObject dbo = converter.convertTo(object);
		collection.insert(dbo);
	}

	@Override
	public long count() {
		return collection.count();
	}

	@Override
	public void findAndModify(DBObject query, T object) {
		DBObject dbo = converter.convertTo(object);
		collection.findAndModify(query, dbo);
	}

	@Override
	public Set<T> findAll(DBObject query, DBObject sort) {
		Set<T> result = new LinkedHashSet<T>();
		DBCursor cursor = collection.find(query).sort(sort);
		while (cursor.hasNext()) {
			T t = converter.convertFrom(cursor.next());
			result.add(t);
		}
		return result;
	}

}
