package com.piku.cloud.dao;

import java.util.Set;

import com.piku.cloud.common.model.Party;

public interface IPartyDAO {

	public long create(Party party);

	public void addCollaborator(long partyId, String collaboratorUsername);

	public Set<Party> loadParties(String collaboratorUsername);

	public Party loadParty(long partyId);

}
