package com.piku.cloud.dao;

import com.piku.cloud.common.model.Photos;

public interface IPhotosDAO {

	public void add(long partyId, String photoUrl);
	
	public Photos fetchPhotos(long partyId);

}
