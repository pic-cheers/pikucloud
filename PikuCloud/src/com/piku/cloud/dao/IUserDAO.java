package com.piku.cloud.dao;

import com.piku.cloud.common.model.User;

public interface IUserDAO {

	public User findOne(String username, String passwordHash);

	public void insert(User user);

	public boolean exists(User user);

	public User findOne(String username);

	public void updateDPPath(String username, String filePath);

}
